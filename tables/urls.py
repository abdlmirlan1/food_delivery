from django.urls import path
from .views import *

urlpatterns = [
    path('', MenuList.as_view()),
    path('category/<str:slug>/', MenuList.as_view()),
    path('category/', CategoryView.as_view()),
    path('<str:slug>/', CategoryItemView.as_view()),

]
