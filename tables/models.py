from django.db import models
from django.template.defaultfilters import slugify


class ProductCategory(models.Model):
    name = models.CharField(max_length=64, blank=True, null=True, default=None)
    slug = models.SlugField(
        default='',
        editable=False,
        max_length=100,
        unique=True,
    )

    def save(self, *args, **kwargs):
        value = self.name
        self.slug = slugify(value)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name

# class ProductManager(models.Manager):
#     def all(self,*args, **kwargs):
#         return super(ProductManager, self).get_queryset().filter(available=True)

class Product(models.Model):
    name = models.CharField(max_length=64)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    category = models.ForeignKey(ProductCategory, default=None, on_delete=models.CASCADE, related_name="products")
    description = models.TextField(blank=True, null=True, default=None)
    is_active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True)
    # Objects = ProductManager()

    def __str__(self):
        return self.name



# class ProductImage(models.Model):
#     product = models.ForeignKey(Product, blank=True, null=True, default=None, on_delete=models.CASCADE)
#     image = models.ImageField(upload_to='products_images/')
#     created = models.DateTimeField(auto_now_add=True, auto_now=False)
#     updated = models.DateTimeField(auto_now_add=False, auto_now=True)
#
#     def __str__(self):
#         return self.image

