from django import forms
from .models import *

class ProductCategoryForm(forms.ModelForm):
    class Meta:
        model = ProductCategory
        fields = ('name')

class ProductForm(forms.ModelForm):
    class Meta:
        models = Product
        fields = ('name', 'is_active', 'description', 'category')

# class ProductImageForm(forms.ModelForm):
#     class Meta:
#         models = Product
#         fiedls = ('image')
