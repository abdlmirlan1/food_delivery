# from django.contrib.auth import authenticate, login
# from django.shortcuts import redirect, render
from rest_framework import generics, filters
# from blog.forms import SignUpForm
from .models import *
from django.http import JsonResponse
from django.core import serializers
from rest_framework.views import APIView

from rest_framework.response import Response
import json
from .serializers import *
from .pagination import *
# from .permissions import IsCommentOwner, IsPostOwner
# from rest_framework.authentication import TokenAuthentication
from django_filters.rest_framework import DjangoFilterBackend
# from .pagination import *

class MenuList(generics.ListAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_backends = (DjangoFilterBackend, filters.SearchFilter)
    filterset_fields = ('name', 'category')
    search_fields = ('name') 
    pagination_class = ListPagination 

   
class CategoryView(generics.ListAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
  
    


class CategoryItemView(APIView):

    def get(self, request, slug):
        category = ProductCategory.objects.get(slug=slug)
        category = category.products.all()
        category_serializer = ProductCategorySerializer(category, many=True)
        return Response(category_serializer.data)
