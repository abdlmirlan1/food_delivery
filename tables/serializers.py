from rest_framework import serializers
from .models import *


class ProductCategorySerializer(serializers.ModelSerializer):

    class Meta:
        fields = ('name', 'slug')
        model = ProductCategory


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        fields = ('id', 'name', 'price', 'category', 'description', 'is_active', 'created', 'updated')
        model = Product


# class ProductImageSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         fields = ('id', 'image', 'created', 'updated')
#         model = ProductImage